//
//  GameElements.swift
//  FlappyBird
//
//  Created by Ataberk Dinç on 4.03.2018.
//  Copyright © 2018 Ata Games. All rights reserved.
//

import SpriteKit

struct CollisionBitMask {
    static let birdCategory:UInt32 = 0x1 << 0
    static let pillarCategory:UInt32 = 0x1 << 1
    static let flowerCategory:UInt32 = 0x1 << 2
    static let groundCategory:UInt32 = 0x1 << 3
}

extension GameScene {
